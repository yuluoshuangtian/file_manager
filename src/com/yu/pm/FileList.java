package com.yu.pm;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class FileList
{
	private SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
	private List<File> list = new ArrayList<File>();
	private JFrame frame;
	private JTextArea outputArea;
	private JScrollPane scrollPane;
	
	public void showListFrame()
	{
		frame = new JFrame("File list");
		outputArea = new JTextArea();
        scrollPane = new JScrollPane(outputArea);
		
		frame.add(scrollPane, BorderLayout.CENTER);
		
		frame.setSize(500, 500);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
		
		frame.setVisible(true);
	}
	
	public void fileTraversing(File parentNode, String leftIndent)
	{
		
		if (parentNode.isDirectory())
		{
			
			leftIndent += "         ";
			
			File childNodes[] = parentNode.listFiles();
			for (File childNode : childNodes)
			{
				fileTraversing(childNode, leftIndent);
			}
		}
		else
		{		
			outputArea.append(String.format
					("%s%-15s%-16s%s%s\n", 
							leftIndent, parentNode.getName(), " is a file.", "Last modified: ", 
							date.format(new Date(parentNode.lastModified()))));
			
			list.add(parentNode);
		}
	}
	
	public File[] getFileList()
	{
		File[] files = list.toArray(new File[0]);
		return files;
	}
}
