package com.yu.pm;

import java.io.File;
import java.text.SimpleDateFormat;

public class PhotoManager
{

	/**
	 * Manage all files from given sourceDirectory(including sub-directories),
	 * by grouping them into separate folders based on creation date.
	 * 
	 * For example, sourceDirectory contains files created both at Aug 2013 and
	 * Sep 2013. As a result, there should be following folder structure created
	 * under targetDirectory:
	 * 
	 * <pre>
	 * targetDirectory
	 * -------2013
	 * ---------------2013-08 (which contains files created in Aug 2013)
	 * ---------------2013-09 (which contains files created in Sep 2013)
	 * </pre>
	 * 
	 * @param sourceDirectory
	 * @param targetDirectory
	 */
	public void manage(File sourceDirectory, File targetDirectory)
	{
		if (sourceDirectory == null || !sourceDirectory.isDirectory())
		{
			throw new IllegalArgumentException("source directory can not be found!");
		}

		// TODO: finish this method

	}

	/**
	 * Utility method to print out the attributes of given file or directory.
	 * 
	 * @param file
	 */
	public void printFileAttribute(File file)
	{
		System.out.println("Path: " + file.getAbsolutePath());
		System.out.println("Name: " + file.getName());
		System.out.println("Type: " + (file.isDirectory() ? "folder" : "file"));
		System.out.println("Modified date: " + new SimpleDateFormat("yyyy-MM-dd").format(file.lastModified()));
		System.out.println("-----------------------------------");
	}

	/**
	 * Utility method to print out the files or directory in a given directory
	 * Recursively.
	 * 
	 * @param diectory
	 */
	public void printFiles(File dirctory)
	{
		if (dirctory.isFile())
		{
			printFileAttribute(dirctory);
		} else
		{
			File[] files = dirctory.listFiles();
			for (File f : files)
			{
				printFiles(f);
			}
		}
	}

	public static void main(String[] args)
	{
		PhotoManager pm = new PhotoManager();
		File sourceDirectory = new File("photos");
		File targetDirectory = new File("managed photos" );
		pm.printFiles(sourceDirectory);

		new PhotoManager().manage(sourceDirectory, targetDirectory);
		

		int a = 1;
		int b =2;
		int c = 3;
		
		if (a<b)
			System.out.print("a<b");
		if (a<c)
			System.out.print("a<c");

	}

}
