package com.yu.pm;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileDateInfo
{
	private File file;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	public FileDateInfo(File file)
	{
		this.file = file;
	}
	
	public String getYear()
	{
		String dateString = dateFormat.format(new Date(file.lastModified())); 
		String year = dateString.substring(0, 4);
		
		return year;
	}
	
	public String getMonth()
	{
		String dateString = dateFormat.format(new Date(file.lastModified())); 
		String month = dateString.substring(5, 7);
		
		return month;
	}
	
	public String getDay()
	{
		String dateString = dateFormat.format(new Date(file.lastModified())); 
		String day = dateString.substring(8, 10);
		
		return day;
	}
}
