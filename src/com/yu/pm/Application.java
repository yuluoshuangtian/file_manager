package com.yu.pm;

import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;

public class Application
{
	public static void main(String arg[]) throws IOException
	{
		JOptionPane.showMessageDialog(null, "Seclect the folder you want to clear up.");
		
		FileChooser chooser = new FileChooser();
		FileList fileList = new FileList();
		
		fileList.showListFrame();
		fileList.fileTraversing(chooser.getFile(false), "");
		
		File[] files = fileList.getFileList();
		
		for (File file : files)
		{
			System.out.println(file.getName());
		}
		
		FileManager fm = new FileManager(files);
		fm.manageFile();
	}
}
