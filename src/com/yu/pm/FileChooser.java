package com.yu.pm;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class FileChooser
{
	public File getFile(boolean directoryOnly)
	{
		JFileChooser fileChooser = new JFileChooser();

		if (directoryOnly == false)
			fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		else
			fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		int result = fileChooser.showOpenDialog(null);

		if (result == JFileChooser.CANCEL_OPTION)
		{
			System.exit(1);
		}

		File file = fileChooser.getSelectedFile();

		if ((file == null) || (file.getName().equals("")))
		{
			JOptionPane.showMessageDialog(null, "Invalid File Name", "Invalid File", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}

		return file;
	}
}
