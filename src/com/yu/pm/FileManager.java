package com.yu.pm;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class FileManager
{
	private File[] files;
	private String parentPath;
	private JFrame frame;
	private JComboBox<String> typeSelectBox;
	private JPanel panel;
	private JTextArea outputArea;
	private String[] manageTypes = {"year", "year-month","year-month-day"};
	
	public FileManager(File[] files)
	{
		this.files = files;
		
		FileChooser directoryChooser = new FileChooser();
		
		JOptionPane.showMessageDialog(null, "Seclect a directory that you want to save the managed files.");
				
		File dirToSave = directoryChooser.getFile(true);
		
		parentPath = dirToSave.getPath();
		
	}

	public void manageFile() throws IOException
	{
		outputArea = new JTextArea();
		
		frame = new JFrame("Choose the manage types");
		panel = new JPanel();
		typeSelectBox = new JComboBox<String> (manageTypes);
		
		outputArea.append(String.format("%s\n%s\n%s\n%s\n", 
				"Please select the way you want to manage your files.",
				"year: files sorted based on the last modified year",
				"year-month: files sorted based on the last modified month",
				"year-month-day: files sorted based on the last modified day"));
		
	
		typeSelectBox.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				String s = (String) typeSelectBox.getSelectedItem();
				int confirmSelection = JOptionPane.showConfirmDialog(null, "Are you sure you want to sort your files based on " + s + "?", "Confirmation", JOptionPane.YES_NO_OPTION);
				if (confirmSelection == JOptionPane.YES_OPTION)
				{
					for (File file : files)
					{
						FileDateInfo fileDateInfo = new FileDateInfo(file);
						String year = fileDateInfo.getYear();
						String path = "";
						File directory;
						Path source;
						Path target;
						String day;
						String month;
						switch (s)
						{
						case "year":

							path = parentPath + "\\" + year;
							break;

						case "year-month":

							month = fileDateInfo.getMonth();
							path = path + "\\" + year + "-" + month;
							break;

						case "year-month-day":

							month = fileDateInfo.getMonth();
							day = fileDateInfo.getDay();
							path = path + "\\" + year + "-" + month + "\\" + year + "-" + month + "-" + day;
							break;
						}
						directory = new File(path);

						if (!directory.exists())
						{
							directory.mkdirs();
						}
						source = file.toPath();
						target = directory.toPath().resolve(file.getName());
						try
						{
							Files.copy(source, target, REPLACE_EXISTING);
						} catch (IOException e1)
						{
							e1.printStackTrace();
						}
						break;

					}

					frame.setVisible(false);
					JOptionPane.showMessageDialog(null, "File management is done!");
				} 
			}
		});
		panel.add(outputArea);
		panel.add(typeSelectBox);
		frame.add(panel);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
		
	}
}
